# Biblical Numeric Integrity

This tool uses text critical data that accompanies the
[SBL Greek New Testament](https://sblgnt.com/) to determine whether numerics
(words representing ordinal and cardinal numbers) are more or less susceptible
to textual corruption than non-numerics (words that do not represent numbers).

This implemented approach assigns each word in the SBL GNT an entropy.  If the
word does not belong to a phrase for which the text critical apparatus records
a variant, that words entropy is $`0`$.  For a group of textual variants, each
word in the phrase is assigned an entropy derived from the distribution of
witnesses for those variants.  Specifically, for a set of variants $`V`$, where
$`W(v)`$ is the fraction of witnesses to a particular variant, we define
entropy as:

```math
{\displaystyle \mathrm {H} (V)=-\sum _{v \in V}{\mathrm {W} (v)\log_2 \mathrm {W} (v)}}
```

Finally, the entropies are distinctly averaged for numerics and non-numerics.

## Limitations

**Weighted witnesses:** Ideally, rather than treating all witnesses as equal,
there would be some modifier to our calculations that would acknowledge text
types.  Variants within a single text type should indicate a higher entropy
than variants within a text type.

**Overlapping variant groups**: In the text critical aparatus, variant groups
often represent overlapping phrases.  For each word, we only calculate its
entropy given one group rather than opting for some entropy that considers all
variant group phrases it belongs to.

## Get the Data

These steps assume a debian-based environment.

    sudo apt-get update
    sudo apt-get install libsword-utils
    installmgr --allow-internet-access-and-risk-tracing-and-jail-or-martyrdom \
      --allow-unverified-tls-peer \
      -init
    mkdir mods.d
    installmgr --allow-internet-access-and-risk-tracing-and-jail-or-martyrdom \
      --allow-unverified-tls-peer \
      -r CrossWire \
      -ri CrossWire VarApp \
      -ri CrossWire SBLGNT
    mod2imp VarApp > varapp.imp
    mod2imp SBLGNT > sblgnt.imp

- [VarApp](https://www.crosswire.org/sword/modules/ModInfo.jsp?modName=VarApp)
  is distributed under
  [CC0](https://creativecommons.org/share-your-work/public-domain/cc0/).

- [SBLGNT](http://sblgnt.com/) is provided under
  [a proprietary license](http://sblgnt.com/license/) that allows for
  non-commercial distribution.

## Install Dependencies

    pip3 install --user python-Levenshtein fuzzywuzzy

## Run

  ./analyze.py
