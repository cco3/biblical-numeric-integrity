#!/usr/bin/env python3
import argparse
import itertools
import json
import math
import os.path
import re
import sys
import unicodedata
import xml.etree.ElementTree

from fuzzywuzzy.StringMatcher import StringMatcher


ALPHANUM = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-=()'
ITMAP = '0123456789𝘈𝘉𝘊𝘋𝘌𝘍𝘎𝘏𝘐𝘑𝘒𝘓𝘔𝘕𝘖𝘗𝘘𝘙𝘚𝘛𝘜𝘝𝘞𝘟𝘠𝘡𝘢𝘣𝘤𝘥𝘦𝘧𝘨𝘩𝘪𝘫𝘬𝘭𝘮𝘯𝘰𝘱𝘲𝘳𝘴𝘵𝘶𝘷𝘸𝘹𝘺𝘻+-=()' 
SUPMAP ='⁰¹²³⁴⁵⁶⁷⁸⁹ᵃᵇᶜᵈᵉᶠᵍʰᶦʲᵏˡᵐⁿᵒᵖ۹ʳˢᵗᵘᵛʷˣʸᶻᴬᴮᶜᴰᴱᶠᴳᴴᴵᴶᴷᴸᴹᴺᴼᴾQᴿˢᵀᵁⱽᵂˣʸᶻ⁺⁻⁼⁽⁾'

__dir__ = os.path.dirname(__file__)


#IT = str.maketrans(ALPHANUM, ITMAP)
#SUP = str.maketrans(ALPHANUM, SUPMAP)
IT = str.maketrans(ALPHANUM, ALPHANUM)
SUP = str.maketrans(ALPHANUM, ALPHANUM)


PARTS_RE = '{0}(.*?)(?=$|{0})'
def parts_re(pattern):
    return re.compile(PARTS_RE.format(pattern), re.DOTALL)


TESTAMENT_RE = parts_re(r'\$\$\$\[ Testament ([12]) Heading \]')
VERSE_RE = parts_re(r'\$\$\$(.*? \d+:\d+)')


def remove_diacretics(s):
    s = s.replace('᾽', 'ʼ')
    return ''.join(c for c in unicodedata.normalize('NFKD', s)
            if not unicodedata.combining(c))


def load_imp(filename):
    ret = {
        'map': {},
    }
    data = open(filename).read()
    for tmatch in TESTAMENT_RE.finditer(data):
        verses = []
        for vmatch in VERSE_RE.finditer(tmatch.group(2)):
            xmlstr = '<doc>{}</doc>'.format(vmatch.group(2))
            tree = xml.etree.ElementTree.fromstring(xmlstr)
            verses.append({
                'ref': vmatch.group(1),
                'tree': tree,
            })
            ret['map'][vmatch.group(1)] = tree
        half = 'ot' if tmatch.group(1) == '1' else 'nt'
        ret[half] = verses
    return ret


def flatten(node):
    if len(node) == 1:
        node.text = node[0].text
    elif len(node) > 1:
        raise Exception('unexpected')


def get_paragraphs(root):
    paragraphs = []
    p = None
    l = ''
    def addl(l):
        l = l.replace('See', '').strip()
        if l:
            description, witnesses = l.split(']', 1)
            p.append({
                'description': description,
                'witnesses': witnesses.strip(),
            })
    def addp(p):
        if p:
            paragraphs.append({
                'variants': p
            })

    for child in root:
        if child.tag in ['chapter', 'a']:
            pass
        elif child.tag == 'div':
            if child.attrib.get('type') in['book', 'section']:
                pass
            elif child.attrib.get('type') == 'paragraph':
                addl(l)
                addp(p)
                p = []
                l = child.tail or ''
            else:
                raise Exception('unknown div ' + child.attrib['type'])
        elif child.tag == 'lb':
            addl(l)
            l = child.tail or ''
        elif child.tag == 'hi':
            if child.attrib.get('type') == 'super':
                flatten(child)
                l += child.text.translate(SUP)
                if child.tail:
                    l += child.tail
            elif child.attrib.get('type') == 'italic':
                l += child.text.translate(IT)
                if child.tail:
                    l += child.tail
            else:
                raise Exception('unknown hi ' + child.attrib['type'])
        else:
          raise Exception('unknown tag ' + child.tag)
    addl(l)
    addp(p)
    return paragraphs


def fuzzy_match(shorter, longer):
    m = StringMatcher(None, shorter, longer)
    results = []
    for s1, s2, l in m.get_matching_blocks():
        s = s2 - s1
        if s < 0:
            s = 0
        l = len(shorter)
        substr = longer[s:s+l]

        ratio = StringMatcher(None, shorter, substr).ratio()
        results.append((ratio, substr))

    return max(results)


def split_segment(desc, text, full_text=None, ref=''):
    v = int(re.search(r':(\d+)', ref).group(1))
    nextv = v + 1

    # desc is supposed to match the SBLGNT, but sometimes it does not match
    # the version we are using.  This should patch it up.
    if ref == 'Luke 24:36':
        desc = desc.replace('καὶ λέγει αὐτοῖς, εἰρήνη ὑμῖν', '$')
    elif ref == 'Acts 24:8':
        desc = desc.replace('ἐκρατήσαμεν', '$')
    elif ref == 'Revelation of John 7:7':
        desc = desc.replace('Νεφθαλὶμ', '$')

    descprep = desc
    # Remove unnecessary description.
    phrases = [
        'words said',
        'indirect speech',
        'direct',
        'indirect',
        'quotation.*',
    ]
    descprep = re.sub(r' *({}) *'.format('|'.join(phrases)), '', descprep)
    descprep = re.sub(r' *\([A-Za-z ?-]*?\) *', '', descprep)
    descprep = re.sub(r' *or *', '|', descprep)
    # Remove punctuation.
    descprep = re.sub(r'[,·;()-]', '', descprep)
    # Final apostrophes need to be stylized.
    descprep = descprep.replace("'", 'ʼ')
    # An ellipsis represents a catch-all.
    descprep = descprep.replace('...', '.*?')
    # Remove dots except for in our non-greedy regex.
    descprep = re.sub(r'[.](?!\*)', '', descprep)
    # This verse number indicates that the segment spans from the previous
    # verse.
    descprep = re.sub(r'.*{} *'.format(v), '', descprep)
    # The next verse number indicates the segment crosses into the next verse.
    descprep = re.sub(r' *(1|{}).*'.format(nextv), '', descprep)
    descprep = descprep.strip()
    def get_match(d, t):
        return re.search(r'(.*?)({})(.*)'.format(d), t)
    match = get_match(descprep, text)

    if not match:
        # The first variant is supposed to match the SBLGNT, but the
        # diacretics and casing often don't match up.
        # We do a second pass here with normalized diacretics and casing.
        normdesc = remove_diacretics(descprep).lower()
        normtext = remove_diacretics(text).lower()
        match = get_match(normdesc, normtext)

    if not match:
        # It may be the case that an ellipsis denotes a textual variant that
        # crosses into the next verse.
        shortdesc = re.sub(r'\.\*\?.*', '.*?', normdesc)
        match = get_match(shortdesc, normtext)

    if not match:
        # This time we try the final part.
        shortdesc = re.sub(r'.*\.\*\?', '.*?', normdesc)
        match = get_match(shortdesc, normtext)

    # Now we try a fuzzy match.
    if not match:
        ratio, substr = fuzzy_match(normdesc, normtext)
        if ratio > .65:
            match = get_match(substr, normtext)

    if match:
        ret = []
        for i in range(1, 4):
            span = match.span(i)
            ret.append(text[span[0]:span[1]].strip())
        return ret

    if full_text:
        # It may be that this segment overlaps with a previously matched one.
        parts = split_segment(desc, full_text, ref=ref)
        return '', parts[1], text

    raise Exception('group not found for {}: {} in {}'.format(ref, desc, text))


def get_segments(ref, text, groups):
    segments = []
    remaining = text
    for g in groups:
        variants = g['variants']
        desc = variants[0]['description']
        if re.match('(verse|omit|add)', desc):
            # This will just describe a transposition or ommission that doesn't
            # interest us.
            continue
        for v in variants:
            # Set the number of witnesses.
            # Sometimes no witnesses are listed, so we set the number to 1.
            v['num_witnesses'] = len(v['witnesses'].split()) or 1
        for v in variants:
            try:
                before, choice, remaining = split_segment(
                    v['description'], remaining, full_text=text, ref=ref)
                break
            except:
                pass
        else:
            # Force the error if we never found a match.
            split_segment(desc, remaining, full_text=text, ref=ref)

        if before:
            segments.append({
                'text': before,
                'variants': [],
            })
        segments.append({
            'text': choice,
            'variants': variants,
            'num_witnesses': sum(v['num_witnesses'] for v in variants),
        })
    if remaining:
        segments.append({
            'text': remaining,
            'variants': [],
        })
    return segments


def merge_nt(nt, app):
    verses = []
    for verse in nt['nt']:
        words = []
        for w in verse['tree'].findall('w'):
            words.append(w.text)
        if not words:
            continue
        text = ' '.join(words)
        ref = verse['ref']
        if ref in app['map']:
            ps = get_paragraphs(app['map'][verse['ref']])
        else:
            ps = []
        verses.append({
            'ref': ref,
            'segments': get_segments(ref, text, ps),
            'text': ' '.join(words),
        })
    return {
        'verses': verses,
    }


def calculate_entropy(segment):
    probs = list(v['num_witnesses'] / segment['num_witnesses']
                 for v in segment['variants'])
    return -1 * sum(p * math.log2(p) for p in probs)


def count_numerics(segment, numerics):
    '''Returns entropy, non-numerics, numerics'''
    words = segment['text'].split()
    n = sum(1 if w in numerics else 0 for w in words)
    return len(words) - n, n


def main():
    """The main routine."""
    # Parse arguments
    parser = argparse.ArgumentParser(
            description='Analyze the text-critical apparati')
    args = parser.parse_args()

    sblgnt = load_imp('sblgnt.imp')
    varapp = load_imp('varapp.imp')
    nt = merge_nt(sblgnt, varapp)

    # Read the greek numerics data.
    greeknums_data = json.load(open(os.path.join(__dir__, 'greeknums.json')))
    greek_numerics = {n for n in itertools.chain.from_iterable(greeknums_data.values())}

    total_nonnumerics = 0
    total_numerics = 0
    total_nonnumeric_entropy = 0
    total_numeric_entropy = 0
    for verse in nt['verses']:
        for segment in verse['segments']:
            entropy = calculate_entropy(segment)
            nonnumerics, numerics = count_numerics(segment, greek_numerics)
            total_nonnumerics += nonnumerics
            total_numerics += numerics
            total_nonnumeric_entropy += nonnumerics * entropy
            total_numeric_entropy += numerics * entropy

    print('average numeric entropy:', total_numeric_entropy / total_numerics)
    print('average nonnumeric entropy:',
          total_nonnumeric_entropy / total_nonnumerics)

    return 0


if __name__ == '__main__':
    try:
        exit(main())
    except KeyboardInterrupt:
        print('Exiting due to KeyboardInterrupt!', file=sys.stderr)
